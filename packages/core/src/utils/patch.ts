import {Component} from "../component";


export const patchAttrs = <T extends object = Record<string, any>>(el: HTMLElement | Component<T>, attrs: T) => {
    if (el instanceof Component) {
        return el.setProps(attrs)
    }

    for (const [name, value] of Object.entries(attrs as Record<string, any>)) {
        if (name == 'style') {
            for (const [styleName, styleValue] of Object.entries(value)) {
                (el.style as Record<string, any>)[styleName] = styleValue;
            }
            continue;
        }

        if (name.startsWith('on')) {
            (el as Record<string, any>)[name] = value;
            continue
        }

        el.setAttribute(name, value)
    }
}
