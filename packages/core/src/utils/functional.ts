

export type Functional<T> = () => T
