import {watch} from "@/reactivity";
import {Sources} from "@/reactivity/inject/types.ts";


export const injectText = (elementFactory: string | Sources<string>) => {
    let el: Node = null as unknown as Node;
    const [element, elementSources] = typeof elementFactory == 'string' ? [() => elementFactory, []] : elementFactory;

    watch(() => {
        const isFirstRender = el == null;
        const newEl = document.createTextNode(element())
        if (!isFirstRender) {
            el.parentElement!.replaceChild(newEl, el)
        }
        el = newEl
    }, elementSources, true)


    return el
}