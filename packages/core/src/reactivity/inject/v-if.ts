import {EmptyComponent} from "@/components";
import {watch} from "@/reactivity";
import {unmount} from "@/hooks";
import {Sources} from "@/reactivity/inject/types.ts";

const createVIf = (element: [boolean, () => HTMLElement]) => {
    const [isRender, elementFactory] = element;
    return isRender ? elementFactory() : new EmptyComponent()
}

export const vIf = (factory: Sources<[boolean, () => HTMLElement]>): HTMLElement => {
    const [element, sources] = factory;

    let [isPreviousRender, f] = element();
    let el = createVIf([isPreviousRender, f]);

    watch(() => {
        const [isRender, elementFactory] = element();

        if (isPreviousRender == isRender) return;
        isPreviousRender = isRender;

        let candidate = createVIf([isRender, elementFactory]);

        unmount(el);
        el.replaceWith(candidate);
        el = candidate;
    }, sources)

    return el;
}