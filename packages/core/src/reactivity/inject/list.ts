import {watch} from "@/reactivity";
import {Sources} from "@/reactivity/inject/types.ts";
import {injectElement} from "@/reactivity/inject/inject-element.ts";

let listIndex = 0;

export const list = (factory: Sources<[string, () => HTMLElement][]>): HTMLElement => {
    let data: [string, HTMLElement][] = [];
    const listName = `list-${listIndex++}`;
    const container = injectElement('slot', {slot: listName}, [])

    const [elements, sources] = factory;

    watch(() => {
        const keydValues = elements();

        const newData: [string, HTMLElement][] = [];

        for (const [key, elementFactory] of keydValues) {
            const oldValue = data.find(([oldKey]) => key == oldKey)

            if (oldValue == undefined) {
                newData.push([key, elementFactory()])
                continue
            }

            newData.push(oldValue)
        }

        data = newData;
        container.replaceChildren(...data.map(e => e[1]))
    }, sources, true)


    return container;
}
