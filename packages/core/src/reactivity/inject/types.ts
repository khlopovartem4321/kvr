import {Functional} from "@/utils/functional.ts";
import {Reactive} from "@/reactivity";

export type Sources<T, R extends object = any> = [Functional<T>, Reactive<R>[]]
