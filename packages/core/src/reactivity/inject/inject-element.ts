import {Component} from "@/component.ts";
import {watch} from "@/reactivity";
import {patchAttrs} from "@/utils/patch.ts";
import {pushQueue, runTasks} from "@/global/async.queue.ts";
import {Sources} from "@/reactivity/inject/types.ts";

export const injectElement = <Props extends object = any>(elementFactory: string | (new () => Component<Props>), propsFactory: Record<string, any> | Sources<Record<string, any>>,
                                                          contentFactory: Node[] = []): HTMLElement => {
    let el = typeof elementFactory == 'string' ? document.createElement(elementFactory) : new elementFactory();
    let isFirstRender = true;

    el.replaceChildren(...contentFactory)

    const [props, propsSources] = Array.isArray(propsFactory) ? propsFactory : [() => propsFactory, []]

    watch(() => {
        patchAttrs(el, props());

        if (isFirstRender && el instanceof Component) {
            isFirstRender = false;
            const result = el.setupElement();
            if (result instanceof Promise) {
                // @ts-ignore
                if (!Object.hasOwn(el.props.value, 'fallback')) {
                    throw new Error("Fallback not provide in async component")
                }

                // @ts-ignore
                if (typeof el.props.value.fallback != 'function') {
                    throw new Error("Typeof fallback must by function")
                }

                pushQueue({
                    asyncInfo: {
                        // @ts-ignore
                        fallback: el.props.value.fallback()
                    },
                    component: el,
                    setup: result
                })
                runTasks()
            }
        }
    }, propsSources, true)

    return el;
}