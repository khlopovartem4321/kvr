import {Reactive} from "../reactive.ts";
import {watch} from "../watch.ts";


export class Computed<T extends object> extends Reactive<T> {
    constructor(func: () => T, deps: Reactive<any>[]) {
        super(func())

        watch(() => {
            this.setValue(func())
        }, deps)
    }
}



export const computed = <T extends object>(func: () => T, deps: Reactive<any>[]): Computed<T> => {
    return new Computed<T>(func, deps)
}
