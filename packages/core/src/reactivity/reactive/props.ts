import {RefValue} from "@/reactivity/reactive/ref.ts";


export class PropsRef<Props> extends RefValue<Props> {}