import {KeyInfo, Reactive} from "../reactive.ts";


const isObject = (value: any): value is object => {
    return Object(value) === value;
}

const createDeepRef = <T extends object>(value: T, keys: string[], callback: (keys: KeyInfo[]) => void) => {
    const res = value;

    for (const [key, current] of Object.entries(value)) {
        if (Array.isArray(current) || isObject(current)) {
            // @ts-ignore
            res[key] = createDeepRef(current, [...keys, keys], callback);
        }
    }

    return new Proxy<T>(res, {
        set(target: T, p: string | symbol, newValue: any, _: any): boolean {
            target[p as keyof T] = newValue;
            callback([...keys, p])

            return true;
        }
    })
}


export class RefValue<T> extends Reactive<T> {
    constructor(value: T) {
        super(value)

        if (isObject(value)) {
            const thisRef = this;
            // @ts-ignore
            this.setValue(createDeepRef(value, [], (keys) => {
                thisRef.triggerCallbacks(keys)
            }), false);
        }
        else {
            this.setValue(value, false)
        }
    }

    set value(newValue: T) {
        this.setValue(newValue, true)
    }

    get value() {
        return super.value;
    }
}


export const ref = <T>(initialValue: T): RefValue<T> => {
    return new RefValue(initialValue)
}
