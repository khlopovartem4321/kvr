import {Reactive} from "@/reactivity/reactive.ts";
import {watch} from "@/reactivity/watch.ts";

type Pr<T> = Exclude<T extends Promise<infer U> ? U: T, undefined>;

export class Flow<T> extends Reactive<T | null> {
    constructor() {
        super(null);
    }

    transform<P>(callback: (value: T, previousFlow: Flow<T>) => P): Flow<Pr<P>> {
        const newFlow = flow<Pr<P>>();

        watch(() => {
            if (!callback) return

            let result = callback(this.value as T, this);

            if (result instanceof Promise) {
                result.then(value => {
                    newFlow.notify(value)
                })
                return
            }

            newFlow.notify(result as Pr<P>)

        }, [this])

        return newFlow;
    }

    notify(...values: (T | undefined)[]) {
        for (const value of values) {
            if (value === undefined) continue;

            this.setValue(value, true)
        }
    }
}

export const flow = <T>() => {
    return new Flow<T>();
}
