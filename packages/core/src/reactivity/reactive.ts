export type KeyInfo = string | Symbol
export type ReactiveCallback = (changes: KeyInfo[]) => void

export class Reactive<T> {
    private callbacks: ReactiveCallback[] = [];
    private _value: T

    constructor(value: T) {
        this._value = value;
    }

    get value(): T {
        return this._value;
    }

    protected setValue(value: T, trigger = true) {
        this._value = value;
        if (trigger) this.triggerCallbacks([])
    }

    protected triggerCallbacks(changes: KeyInfo[]) {
        for (const callback of this.callbacks) {
            callback(changes);
        }
    }

    addCallback(callback: ReactiveCallback) {
        this.callbacks.push(callback);
    }

    removeCallback(callback: ReactiveCallback) {
        this.callbacks.splice(this.callbacks.indexOf(callback), 1)
    }
}
