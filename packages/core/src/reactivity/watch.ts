import {Reactive} from "./reactive";
import {onUnMounted} from "../hooks";
import {getCurrentContext} from "@/component.ts";

const isReactive = (source: any): source is Reactive<any> => {
    return source instanceof Reactive;
}

export const watch = (callback: () => void, sources: any[], instantly = false) => {
    const realSourceList = sources.filter(e => isReactive(e)) as Reactive<any>[];

    if (instantly) {
        callback()
    }

    for (const source of realSourceList) {
        source.addCallback(callback)
    }

    if (getCurrentContext() == null) return;

    onUnMounted(() => {
        for (const source of realSourceList) {
            source.removeCallback(callback)
        }
    })
}
