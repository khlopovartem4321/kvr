import {Component, runInCtx} from "../component.ts";

export interface AsyncInfo {
    fallback: Component | HTMLElement
}

export interface AsyncQueueItem {
    setup: Promise<() => HTMLElement[]>;
    component: Component;
    asyncInfo: AsyncInfo
}

export const asyncQueue: AsyncQueueItem[] = []

export const pushQueue = (func: AsyncQueueItem) => {
    asyncQueue.push(func);
}

export const runTasks = async () => {
    while (asyncQueue.length > 0) {
        const task = asyncQueue.shift()

        if (task == undefined) continue;

        try {
            const elements = await task.setup

            runInCtx(() => {
                task.component.addElements(elements())
            }, task.component)
        }
        catch (e) {
            runInCtx(() => {
                task.component.replaceWith(task.asyncInfo.fallback);
            }, task.component)

        }
    }

    asyncQueue.slice(0)
}
