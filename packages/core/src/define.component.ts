import {Component} from "./component";
import {Functional} from "./utils/functional";

export interface ComponentDefinition<Props extends object> {
    name: string;
    setup: (ctx: Component<Props>) => (HTMLElement[] | Promise<Functional<HTMLElement[]>>)
    styles: string[]
}


export const defineComponent = <Props extends object>(component: ComponentDefinition<Props>): new () => Component<Props> => {
    const componentClass = class extends Component<Props> {
        createStyles(): string {
            return component.styles.join('\n');
        }

        setup(ctx: Component<Props>) {
            return component.setup(ctx);
        }
    }
    customElements.define(component.name, componentClass)

    return componentClass;
}
