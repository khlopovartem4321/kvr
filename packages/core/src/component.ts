import {Functional} from "./utils/functional";
import {PropsRef} from "@/reactivity/reactive/index.ts";
import {LifeCycleHook, runHooks} from "@/hooks";

let currentComponent: Component | null;

export const getCurrentContext = () => {
    return currentComponent;
}

export const setCurrentContext = (ctx: Component | null) => {
    currentComponent = ctx;
}

export const runInCtx = <T>(func: () => T, ctx: Component) => {
    const prevCtx = getCurrentContext();
    setCurrentContext(ctx)

    const res = func()

    setCurrentContext(prevCtx)

    return res
}

export abstract class Component<Props extends object = any> extends HTMLElement {
    props: PropsRef<Props>
    private mounted: LifeCycleHook[] = []
    private unmounted: LifeCycleHook[] = []

    setProps(props: Props) {
        this.props.value = props;
    }

    constructor() {
        super();
        this.attachShadow({
            mode: 'open'
        })

        this.props = new PropsRef<Props>(null as unknown as Props);
    }

    abstract setup(ctx: Component<Props>): HTMLElement[] | Promise<Functional<HTMLElement[]>>

    abstract createStyles(): string

    addElements(element: HTMLElement[]) {
        this.shadowRoot!.append(...element)
        this.runMountedHooks()
    }

    setupElement(): void | Promise<() => HTMLElement[]> {
        const styles = new CSSStyleSheet();
        styles.replaceSync(this.createStyles() ?? '');
        this.shadowRoot!.adoptedStyleSheets = [...document.adoptedStyleSheets, styles]

        return runInCtx(() => {
            const res = this.setup(this)

            if (res instanceof Promise) {
                return res
            }

            this.addElements(res)
        }, this)
    }

    runMountedHooks() {
        runHooks(this.mounted)
    }

    runUnMountedHooks() {
        runHooks(this.unmounted)
    }

    addMountedHook(data: LifeCycleHook) {
        this.mounted.push(data)
    }

    addUnMountedHook(data: LifeCycleHook) {
        this.unmounted.push(data)
    }
}
