import {defineComponent} from "../define.component.ts";

export const EmptyComponent = defineComponent({
    name: 'empty-component',
    setup(_){
        return []
    },
    styles: []
})