import {getCurrentContext} from "@/component.ts";


export const onMounted = (action: () => void | Promise<void>) => {
    const context = getCurrentContext()

    if (context == null) {
        throw new Error("Context in null")
    }

    context.addMountedHook(action)
}
