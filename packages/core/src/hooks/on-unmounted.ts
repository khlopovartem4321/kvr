import {Component, getCurrentContext} from "../component";


export const unmount = (element: Element) => {
    if (element instanceof Component) {
        for (const child of element.shadowRoot!!.children) {
            unmount(child);
        }
        element.runUnMountedHooks();
    }

    for (const child of element.children) {
        unmount(child);
    }
}

export const onUnMounted = (action: () => void | Promise<void>) => {
    const context = getCurrentContext()

    if(context == null) {
        throw new Error("Context in null")
    }

    context.addUnMountedHook(action)
}
