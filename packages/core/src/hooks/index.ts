
export type LifeCycleHook = () => void | Promise<void>

export const runHooks = (hooks: LifeCycleHook[]) => {
    for (const hook of hooks) {
        hook()
    }
}

export * from './on-mounted';
export * from './on-unmounted';
