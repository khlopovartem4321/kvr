import {defineConfig} from "tsup";

export default defineConfig({
    entry: ['src/**/*.ts'],
    format: ['esm'],
    dts: true,
    sourcemap: true,
    clean: true,
    minify: false,
    target: 'es2020',
    outDir: 'dist',
    skipNodeModulesBundle: true,
    splitting: false,
    treeshake: {
        moduleSideEffects: false,
    },
})
