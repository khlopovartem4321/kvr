import ts from "typescript";
import {createElementNodes, parseTags, resolveCss} from "@/html";
import {createDefineComponent, createElements, splitCodeAndDeps} from "@/typescript";
import {createFileFromStatements, createImport, getCamelCaseString} from "@kvrjs/utils";
import { join } from "path";

const getPropsType = (propsType: string, target: ts.ScriptTarget) => {
    if (!propsType) {
        throw new Error("Props type not exists")
    }

    const propsTypeInfo = ts.createSourceFile('', `type _Props_ = ${propsType}`, target);
    if (propsTypeInfo.statements.length != 1) {
        throw new Error("Props type info not correct")
    }

    const propsInfo = propsTypeInfo.statements[0]

    if (!ts.isTypeAliasDeclaration(propsInfo)) {
        throw new Error("Props type must by TypeLiteral or TypeReference")
    }

    return propsInfo;
}

export default async (source: string, componentName: string, pathToComponent: string, target: ts.ScriptTarget) => {
    const tags = parseTags(source);
    const style = resolveCss(tags.style)

    const [imports, code] = splitCodeAndDeps(ts.createSourceFile("", tags.script.innerHTML, target))

    const propsInfo = getPropsType(tags.script.attributes['props'], target);

    return {
        ts: createFileFromStatements('', [
            createImport('@kvrjs/core', ['defineComponent', 'injectElement', 'injectText', 'vIf', 'list']),
            ...Object.keys(style).map(key => createImport(join(pathToComponent,`${componentName}.${key}?inline`), getCamelCaseString(`${componentName}-${key}`))),
            ...imports,
            createDefineComponent(componentName, style, code, createElements(createElementNodes(tags.template)), 'async' in tags.script.attributes, propsInfo.type)
        ], target),
        css: style,
    }
}