import {HTMLElement} from "node-html-parser";


export const resolveCss = (csss: HTMLElement[]) => {
    const items: Record<string, string[]> = {}

    for (const item of csss) {
        const key = item.attributes.type ?? 'css';
        const candidate = items[key] ?? [];

        candidate.push(item.innerHTML);
        items[key] = candidate;
    }

    return items;
}
