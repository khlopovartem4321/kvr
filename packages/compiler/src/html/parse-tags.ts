import {HTMLElement, parse, TextNode} from "node-html-parser";


export const parseTags = (text: string) => {
    const tags = parse(text);

    let scriptTag: HTMLElement[] = [], templateTag: HTMLElement[] = [], styleTags: HTMLElement[] = [];

    tags.childNodes.forEach((tag) => {

        if (!(tag instanceof HTMLElement)) {
            if (tag instanceof TextNode) {
                if (tag.rawTagName.replaceAll("\n", "").length == 0) return;
            }
            throw new Error("Данный тег не разрешен в корне элемента")
        }

        if (tag.rawTagName == 'script') {
            scriptTag.push(tag)
            return
        }

        if (tag.rawTagName == 'style') {
            styleTags.push(tag)
            return
        }

        if (tag.rawTagName == 'template') {
            templateTag.push(tag)
            return
        }

        throw new Error("Данный тег не разрешен в корне элемента")
    })

    if (scriptTag.length > 1 || scriptTag.length == 0) {
        throw new Error("количество тегов script не гуд")
    }

    if (templateTag.length > 1 || templateTag.length == 0) {
        throw new Error("количество тегов template не гуд")
    }

    return {
        script: scriptTag[0],
        template: templateTag[0],
        style: styleTags
    }
}
