import ts from "typescript";
import {HTMLElement, TextNode} from "node-html-parser";

export interface ElementNode {
    name: string;
    tagName: ts.Expression;
    props: [string, boolean, string][]
    children: (ElementNode | string)[]
    slot: Record<string, ElementNode>
    for?: [string, string, string]
    if?: string
    slotName?: string
}

export const createElementNodes = (template: HTMLElement): (ElementNode | string)[] => {
    const result: (ElementNode | string)[] = []

    for (const node of template.childNodes) {
        if (node instanceof TextNode) {
            if (node.innerText.trim().length == 0) continue;

            result.push(node.innerText)
        } else if (node instanceof HTMLElement) {
            const props: [string, boolean, string][] = []
            let forStatement: [string, string, string] | undefined = undefined;
            let ifStatement: string | undefined = undefined;
            let slotName: string | undefined = undefined;
            let key: string | undefined = undefined;

            for (const [propName, propValue] of Object.entries(node.attrs)) {
                const isReactive = propName.startsWith(":");

                if (propName == 'for') {
                    const [itemName, bodyName] = propValue.split(" in ");
                    forStatement = [itemName, bodyName, ''];
                    continue
                }

                if (propName == 'if') {
                    ifStatement = propValue
                    continue
                }


                if (propName.toLowerCase() == 'slotname') {
                    slotName = propValue;
                }

                if (propName == 'key') {
                    key = propValue;
                }

                props.push([propName.substring(isReactive ? 1 : 0), isReactive, propValue])
            }

            const candidateChildren = createElementNodes(node);


            if (forStatement) {
                if (!key) {
                    throw new Error("key is not define from for prop")
                }

                forStatement = [forStatement[0], forStatement[1], key];
            }


            result.push({
                name: node.rawTagName,
                tagName: node.rawTagName.toLowerCase() == node.rawTagName ? ts.factory.createStringLiteral(node.tagName) : ts.factory.createIdentifier(node.rawTagName),
                props: props,
                for: forStatement,
                if: ifStatement,
                children: candidateChildren.filter(item => typeof item == 'string' || (item.slotName == undefined)),
                slot: Object.fromEntries(
                    candidateChildren.filter(item => typeof item != 'string' && item.slotName != undefined)
                        .map(e => typeof e != "string" ? [e.slotName, e] : ['error', null as unknown as ElementNode])
                ),
                slotName: slotName,
            })
        }
    }


    return result;
}