
export const resolveScript = (text: string): [string[], string[]] => {
    let imports: string[] = [];

    const lines = text.split("\n");

    let index = 0;
    for (;index < lines.length; index++) {
        const line = lines[index]

        if (line.startsWith("import")) {
            imports.push(line)
            continue;
        }

        break;
    }

    return [imports, lines.filter((_, currentIndex) => currentIndex > index)];
}
