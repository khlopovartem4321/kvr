export interface SplitedExpression {
    text: string;
    isComputed: boolean
}

export const splitExpression = (text: string): SplitedExpression[] => {
    const regex = /{{(.*?)}}/g;
    const expressions: SplitedExpression[] = [];
    let lastIndex = 0;
    let match;

    while ((match = regex.exec(text)) !== null) {
        const beforeMatch = text.substring(lastIndex, match.index);
        if (beforeMatch.trim() !== '') {
            expressions.push({text: beforeMatch.trim(), isComputed: false});
        }
        expressions.push({text: match[1].trim(), isComputed: true});
        lastIndex = regex.lastIndex;
    }

    const remainingText = text.substring(lastIndex).trim();
    if (remainingText !== '') {
        expressions.push({text: remainingText, isComputed: false});
    }

    return expressions;
}
