import ts, {ImportDeclaration, SourceFile} from "typescript";


export const splitCodeAndDeps = (codeStatements: SourceFile): [ImportDeclaration[], ts.SourceFile] => {
    const imports: ImportDeclaration[] = []

    ts.forEachChild(codeStatements, (node) => {
        if (ts.isImportDeclaration(node)) {
            imports.push(node);
        }
    })

    function transformer<T extends ts.Node>(context: ts.TransformationContext) {
        return (rootNode: T) => {
            return ts.visitNode(rootNode, (sourceFile: ts.Node) => {
                return ts.visitEachChild(sourceFile, ((node) => {
                    if (ts.isImportDeclaration(node)) {
                        if (imports.includes(node)) return undefined;
                    }

                    return ts.visitEachChild(node, visitChild, context);

                    function visitChild(child: ts.Node) {

                        return ts.visitEachChild(child, visitChild, context);
                    }
                }), context);
            });
        }
    }

    const result = ts.transform(codeStatements, [transformer]);

    return [imports, result.transformed[0] as ts.SourceFile]
}
