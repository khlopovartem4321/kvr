import ts, {Expression} from "typescript";
import {createTexts, getVariables} from "./create-texts";
import {createSourceValue} from "./create-source-value";
import {ElementNode} from "../html";
import {callFunction, createArrow, createObject} from "@kvrjs/utils";


const createVIfExpression = (expression: ts.Expression, propValue: string) => {
    return callFunction(
        'vIf',
        [
            createSourceValue(
                ts.factory.createArrayLiteralExpression([
                    ts.factory.createIdentifier(propValue),
                    createArrow([], [ts.factory.createReturnStatement(expression)])
                ]),
                getVariables(propValue)
            ),
        ]
    )
}

const createVForExpression = (expression: ts.Expression, itemName: string, collectionName: string, key: string) => {
    return ts.factory.createCallExpression(
        ts.factory.createIdentifier('list'),
        undefined,
        [
            createSourceValue(
                callFunction(
                    ts.factory.createPropertyAccessExpression(ts.factory.createIdentifier(collectionName), ts.factory.createIdentifier('map')),
                    [createArrow([
                        ts.factory.createParameterDeclaration(undefined, undefined, itemName)
                    ], [
                        ts.factory.createReturnStatement(
                            ts.factory.createArrayLiteralExpression([
                                ts.factory.createIdentifier(
                                    key
                                ),
                                ts.factory.createArrowFunction(undefined, undefined, [], undefined, ts.factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
                                    ts.factory.createBlock([ts.factory.createReturnStatement(expression)]))
                            ])
                        )
                    ])]
                ),
                getVariables(collectionName)
            )
        ]
    )
}

export const createElements = (template: (ElementNode | string)[]): Expression[] => {
    const result: Expression[] = []

    for (const node of template) {
        if (typeof node == 'string') {
            result.push(...createTexts(node))
            continue;
        }

        const props = Object.fromEntries(
            node.props.map<[string, Expression]>(
                ([name, isCalculating, value]) =>
                    [name, isCalculating ? ts.factory.createIdentifier(value) : ts.factory.createStringLiteral(value)]
            )
        )

        if (node.name === 'slot') {
            const slotName = node.props.find(([name, _, _1]) => name == 'name')

            if (!slotName) {
                throw new Error("SlotName not provided")
            }

            if (slotName[1]) {
                throw new Error("SlotName cant be dynamic")
            }

            result.push(
                ts.factory.createCallExpression(
                    ts.factory.createPropertyAccessExpression(
                        ts.factory.createPropertyAccessExpression(
                            ts.factory.createIdentifier("props"),
                            ts.factory.createIdentifier("value")
                        ),
                        ts.factory.createIdentifier(slotName[2])
                    ),
                    undefined,[createObject(props)]
                )
            )

            continue;
        }


        const slot = Object.fromEntries(
            Object.entries(node.slot).map(([key, value]) => {

                const slotPropsFieldName = value.props.find(([name, _, _1]) => name === 'slotprops')

                return [key, createArrow(slotPropsFieldName ? [
                    ts.factory.createParameterDeclaration(undefined, undefined, slotPropsFieldName[2])
                ]: [], [
                    ts.factory.createReturnStatement(
                        createElements([value])[0]
                    )
                ])];
            }));



        let resultNode = callFunction(
            'injectElement',
            [
                node.tagName,
                createSourceValue(
                    createObject(
                        {
                            ...props,
                            ...slot,
                        },
                    ),
                    [
                        ...node.props.flatMap(([_, _1, propValue]) => getVariables(propValue)),
                    ]
                ),
                ts.factory.createArrayLiteralExpression(
                    createElements(node.children)
                )
            ]
        )

        if (node.for) {
            resultNode = createVForExpression(resultNode, node.for[0], node.for[1], node.for[2]);
        }

        if (node.if) {
            resultNode = createVIfExpression(resultNode, node.if)
        }

        result.push(resultNode)
    }

    return result;
}
