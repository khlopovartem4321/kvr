import ts, {Expression} from "typescript";
import {splitExpression} from "../split-expression";
import {getChildrenCount} from "./get-children-count";
import {callFunction, createArrow} from "@kvrjs/utils";

export const getVariables = (text: string): (ts.Identifier | ts.PropertyAccessExpression)[] => {
    const node = ts.createSourceFile("", text, ts.ScriptTarget.Latest, true).statements[0] as ts.ExpressionStatement;
    const list: (ts.Identifier | ts.PropertyAccessExpression)[] = [];

    function visitNode(n: ts.Node) {
        if (ts.isPropertyAccessExpression(n)) {
            list.push(n)
            if (getChildrenCount(n.expression) == 0 && ts.isIdentifier(n.expression)) {
                list.push(n.expression)
            }
        }

        n.forEachChild(visitNode);
    }

    visitNode(node)

    return list;
}

export const createTexts = (text: string): Expression[] => {
    const expressions = splitExpression(text)

    return expressions.map(e => {
        let elements: (ts.Identifier | ts.PropertyAccessExpression)[] = []

        if (e.isComputed) {
            elements = getVariables(e.text)
        }

        return callFunction(
            'injectText',
            [
                e.isComputed ?
                    ts.factory.createArrayLiteralExpression(
                        [
                            createArrow([], [
                                ts.factory.createReturnStatement(ts.factory.createIdentifier(e.text))
                            ]),
                            ts.factory.createArrayLiteralExpression(
                                elements
                            )
                        ]
                    )
                    : ts.factory.createStringLiteral(e.text)
            ]
        )
    })

}
