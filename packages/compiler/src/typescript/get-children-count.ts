import ts from "typescript";

export const getChildrenCount = (node: ts.Node): number => {
    let count = 0;
    ts.forEachChild(node, (n: ts.Node) => {
        count++;
        count += getChildrenCount(n);
    });
    return count;
}
