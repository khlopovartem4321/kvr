
import ts, {Expression} from "typescript";


export const createSourceValue = (body: Expression, sources: Expression[]) => {
    return ts.factory.createArrayLiteralExpression([
        ts.factory.createArrowFunction(undefined, undefined, [], undefined, ts.factory.createToken(ts.SyntaxKind.EqualsGreaterThanToken),
            ts.factory.createBlock([
                ts.factory.createReturnStatement(
                    body
                )
            ])),
        ts.factory.createArrayLiteralExpression(sources)
    ])
}