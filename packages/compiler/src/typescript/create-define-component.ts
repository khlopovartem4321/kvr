import ts, {Expression} from "typescript";
import {callFunction, createArrow, createExport, createObject, getCamelCaseString} from "@kvrjs/utils";

export const createDefineComponent = (componentName: string, style: Record<string, string[]>, body: ts.SourceFile, elements: Expression[], isAsync: boolean, propsType: ts.TypeNode) => {
    const returnStatement = ts.factory.createReturnStatement(ts.factory.createArrayLiteralExpression(elements));

    return createExport(
        callFunction(
            "defineComponent",
            [
                createObject({
                    name: ts.factory.createStringLiteral(componentName),
                    "setup": createArrow([
                        ts.factory.createParameterDeclaration(undefined, undefined, 'ctx', undefined, undefined, undefined)
                    ],
                        [
                        ts.factory.createVariableStatement(
                            undefined,
                            ts.factory.createVariableDeclarationList(
                                [ts.factory.createVariableDeclaration(
                                    ts.factory.createIdentifier("props"),
                                    undefined,
                                    undefined,
                                    ts.factory.createPropertyAccessExpression(
                                        ts.factory.createIdentifier("ctx"),
                                        ts.factory.createIdentifier("props")
                                    )
                                )],
                                ts.NodeFlags.Const
                            )
                        ),

                        ...body.statements,

                        isAsync ?
                            ts.factory.createReturnStatement(
                                createArrow([], [returnStatement], false)
                            ) :
                            returnStatement,
                    ], isAsync),
                    "styles": ts.factory.createArrayLiteralExpression([
                        ...Object.keys(style).map((key) => ts.factory.createIdentifier(getCamelCaseString(`${componentName}-${key}`)))
                    ])
                })
            ],
            [
                propsType,
            ]
        )
    )
}
