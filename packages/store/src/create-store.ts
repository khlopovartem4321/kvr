import {computed as createComputed, ref} from "@kvrjs/core";
import {AnyRecord, Store, StoreInfo} from "@/types.ts";

// @ts-ignore
const IS_DEV = !import.meta.env.PROD

export const createStore = <State,
    Methods extends Record<string, Function>,
    Computed extends Record<string, () => any>>
(stateInfo: StoreInfo<State, Methods, Computed>): Store<State, Methods, Computed> => {
    const state = ref<State>({
        ...stateInfo.initialValue(),
    })
    const methods = stateInfo.methods(state);
    const computed = Object.fromEntries(
        Object.entries(stateInfo.computed(state))
            .map(([key, value]) => [key, createComputed(value, [state])]
            )
    ) as AnyRecord<Computed>

    const resultStore = {
        name: stateInfo.name,
        state,
        methods,
        computed
    }

    if (IS_DEV) {
        if (!('stores' in window)) {
            // @ts-ignore
            window.stores = [];
        }

        // @ts-ignore
        window.stores.push(resultStore)
    }

    return resultStore;
}