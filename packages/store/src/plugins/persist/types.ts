import {RefValue} from "@kvrjs/core";

export interface PersistStoreProvider<State> {
    saveTo: (state: Partial<State>) => void
    load: () => Partial<State>
}

export interface SavingStrategy<State, SerializedState = Partial<State>, DeserializedState = Partial<State>> {
    serialize: (state: Partial<State>) => SerializedState
    merge: (state: RefValue<State>, deserializedState: DeserializedState) => void
}


export interface PersistOptions<State, SerializedState = Partial<State>, DeserializedState = Partial<State>> {
    provider: (name: string) => PersistStoreProvider<State>
    saveStrategy: () => SavingStrategy<State, SerializedState, DeserializedState>
}
