import {Store} from "@/types.ts";
import {PersistOptions, PersistStoreProvider, SavingStrategy} from "@/plugins/persist/types.ts";
import {watch} from "@kvrjs/core";

export const createLocalStorageProvider = <State>(name: string): PersistStoreProvider<State> => {
    return {
        saveTo: (state) => {
            localStorage.setItem(name, JSON.stringify(state))
        },
        load() {
            return JSON.parse(localStorage.getItem(name) ?? '{}') as Partial<State>
        }
    }
}

export const defaultSaveStrategy = <State>(): SavingStrategy<State> => {
    return {
        merge: (state, deserializedState) => {
            Object.entries(deserializedState).forEach(([key, value]) => {
                // @ts-ignore
                state.value[key] = value;
            })
        },
        serialize: (state) => state
    }
}


export const persist = <State,
    Methods extends Record<string, Function>,
    Computed extends Record<string, () => any>>
(store: Store<State, Methods, Computed>, options?: Partial<PersistOptions<State>>) => {
    const provider = (options?.provider ?? createLocalStorageProvider)(store.name)
    const saveStrategy = (options?.saveStrategy ?? defaultSaveStrategy)()

    saveStrategy.merge(store.state, provider.load())

    watch(() => {
        const serializedState = saveStrategy.serialize(store.state.value);
        provider.saveTo(serializedState);
    }, [store.state])


    return store;
}
