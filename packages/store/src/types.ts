import {Computed, RefValue} from "@kvrjs/core";

export type AnyRecord<T extends Record<string, () => any>> = { [Key in keyof T]: Computed<ReturnType<T[Key]>> };

export interface StoreInfo<State, Methods, Computed> {
    name: string;
    initialValue: () => State;
    methods: (state: RefValue<State>) => Methods
    computed: (state: RefValue<State>) => Computed
}

export interface Store<State, Methods extends Record<string, Function>, Computed extends Record<string, () => any>> {
    state: RefValue<State>
    computed: AnyRecord<Computed>
    methods: Methods
    name: string
}