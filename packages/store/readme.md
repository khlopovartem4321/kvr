# @kvrjs/store

### Дополнение к фреймворку KVR
### Реализует сторы

# Установка 

```bash
npm install @kvrjs/store
```

# Использование

```js
import {createStore} from '@kvrjs/store'

export const store =
    createStore({
        methods: (state) => ({
            increment: () => {
                state.value.count++;
            }
        }),
        name: 'data',
        initialValue: () => ({
            count: 0
        }),
        computed: (state) => ({
            double: () => state.value.count * 2
        })
    })
```

В компоненте

```
<script>
import {store} from './store';
</script>

<template>
    <div>{{store.state.value.count}}</div>
    <button :onclick="store.methods.increment">Update</button>
    <div>{{store.computed.double.value}}</div>
</template>
```

## Так же есть дополнения/плагины

### persist
```js
import {createStore, persist} from '@kvrjs/store'

export const store = persist(
    createStore({
        methods: (state) => ({
            increment: () => {
                state.value.count++;
            }
        }),
        name: 'data',
        initialValue: () => ({
            count: 0
        }),
        computed: (state) => ({
            double: () => state.value.count * 2
        })
    })
)
```