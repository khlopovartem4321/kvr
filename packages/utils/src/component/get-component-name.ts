
const EXT_NAME = 'kvr'

export const getFilePathWithoutExt = (file: string) => {
    return file.substring(0, file.length - EXT_NAME.length - 1);
}

export const getComponentName = (file: string) => {
    const fileNameWithExt = file.split("/").at(-1) as string;
    const fileName = getFilePathWithoutExt(fileNameWithExt);
    const componentName = fileName!.replaceAll(".", "-").toLowerCase()

    return componentName.endsWith('component') ? componentName : `${componentName}-component`;
};

export const getPageComponentName = (file: string, exclude: string) => {
    const fileNameWithExt = file
        .substring(exclude.length)
        .replaceAll("/", "-")
        .replaceAll("[", "")
        .replaceAll("]", "")

    const fileNameWithoutExt =  getFilePathWithoutExt(fileNameWithExt);
    const componentName = fileNameWithoutExt!.replaceAll(".", "-").toLowerCase()

    return componentName.endsWith('component') ? componentName : `${componentName}-component`;
}

export const getCamelCaseString = (str: string) => {
    return str.replaceAll(/-(\w)/g, (_, letter) => letter.toUpperCase());
}