import ts, {ScriptTarget, Statement} from "typescript";


export const createFileFromStatements = (name: string, statements: Statement[], target: ScriptTarget) => {
    let module = ts.createSourceFile(name, '', target);

    return ts.factory.updateSourceFile(module, statements, true);

}