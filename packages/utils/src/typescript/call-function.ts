import ts, {Expression} from "typescript";


export const callFunction = (name: string | Expression, args: Expression[], node: ts.TypeNode[] = []) => {
    return ts.factory.createCallExpression(
        typeof name == 'string' ? ts.factory.createIdentifier(name): name,
        node,
        args
    );
}


