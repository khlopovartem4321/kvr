import ts, {ParameterDeclaration, Statement} from "typescript";


export const createArrow = (params: ParameterDeclaration[], content: Statement[], isAsync = false) => {
    return ts.factory.createArrowFunction(
        isAsync ? [ts.factory.createModifier(ts.SyntaxKind.AsyncKeyword)]: [],
        undefined,
        params,
        undefined,
        undefined,
        ts.factory.createBlock(content)
    )
}