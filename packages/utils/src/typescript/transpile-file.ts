import ts, {CompilerOptions, SourceFile} from "typescript";


export const transpileFile = (source: SourceFile, config: CompilerOptions) => {
    const printer: ts.Printer = ts.createPrinter();

    return ts.transpileModule(printer.printFile(source), {
        compilerOptions: config
    }).outputText;
}