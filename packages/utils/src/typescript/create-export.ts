import ts, {Expression} from "typescript";


export const createExport = (value: Expression) => {
    return ts.factory.createExportAssignment(
        undefined,
        false,
        value
    )
}