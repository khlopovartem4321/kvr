import ts from "typescript";


export const createImport = (from: string, symbol: string | string[]) => {
    return ts.factory.createImportDeclaration(undefined,
        ts.factory.createImportClause(
            false,
            typeof symbol == 'string' ? ts.factory.createIdentifier(symbol): undefined,
            typeof symbol == 'string'? undefined: ts.factory.createNamedImports([
                ...symbol.map(e => ts.factory.createImportSpecifier(false, undefined, ts.factory.createIdentifier(e)))
            ])
        ),
        ts.factory.createStringLiteral(from)
    )
}