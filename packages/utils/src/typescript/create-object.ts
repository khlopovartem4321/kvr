import ts from "typescript";

export const createObject = (args: Record<string, ts.Expression>) => {
    return ts.factory.createObjectLiteralExpression(
        Object.entries(args).map(([key, value]) => ts.factory.createPropertyAssignment(ts.factory.createIdentifier(key), value))
    )
}