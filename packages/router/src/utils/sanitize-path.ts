export const sanitizePath = (path: string) => {
    const substrStartPath = path.startsWith('/') ? path.slice(1) : path;
    const substrEndPath = substrStartPath.endsWith('index') ? substrStartPath.slice(0, substrStartPath.length - 5) : substrStartPath;
    return substrEndPath.endsWith('/') ? substrEndPath.substring(0, substrEndPath.length - 1) : substrEndPath;
}
