import {getPageParams} from "@/utils/get-page-params.ts";
import {sanitizePath} from "@/utils/sanitize-path.ts";

export const compare = (candidate: string, location: string): [boolean, Record<string, string>] => {
    const [candidatePath, keyArgs] = getPageParams(`/${sanitizePath(candidate)}`);

    const resultValues = new RegExp(`^${candidatePath}`).exec(location);

    if (resultValues) {
        const entries = keyArgs.map((e, index) => {
            return [e, resultValues[index + 1]]
        })

        return [true, Object.fromEntries(entries)]
    }

    return [false, {}]
}
