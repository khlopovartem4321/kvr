
export const regex = /\[(.*?)]/g;


export const getPageParams = (path: string): [string, string[]] => {
     return [
        path.replaceAll(regex, '(.*?)'),
        path.match(regex)?.map(e => e.slice(1, -1)) || []
    ]
}