import {RouterElement} from "@/types.ts";
import {compare} from "@/utils";
import {Component, ref} from "@kvrjs/core";

export const navigation = ref<(path: string, search: Record<string, string>) => void>(() => {})

export const createRouter = (pages: RouterElement[], container: HTMLElement) => {
    let el: HTMLElement | null = null;

    navigation.value = (path: string, search: Record<string, string>) => {
        const searchString = Object.entries(search).map(e => `${e[0]}=${e[1]}`).join('&');
        window.history.pushState({}, '', `${path}${searchString.length == 0 ? '': '?'}${searchString}`)
        for (const candidatePage of pages) {
            const [isIdentity, args] = compare(candidatePage.path, path);
            if (isIdentity) {
                const newEl = candidatePage.element();

                if (el != null) {
                    if (el instanceof Component) {
                        el.runUnMountedHooks()
                    }
                }

                container.replaceChildren(newEl)

                if (newEl instanceof Component) {
                    newEl.setProps({
                        ...args,
                        ...search,
                    });
                    newEl.setupElement();
                }
                el = newEl;
                return;
            }
        }

        throw new Error("Page not found")
    }

    const navigateEvent = () => {
        navigation.value(location.pathname, Object.fromEntries(new URLSearchParams(location.search).entries()));
    };

    window.onpopstate = navigateEvent;
    navigateEvent();
}
