import {navigation} from "@/create-router.ts";


export const useNavigate = () => {
    return navigation;
}