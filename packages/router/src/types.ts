import {Component} from "@kvrjs/core";

export interface RouterElement {
    path: string;
    element: () => HTMLElement | Component;
}