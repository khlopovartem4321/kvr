#!/usr/bin/env node
import prompts from "prompts";
import {dirname, join} from "path";
import {fileURLToPath} from "node:url";
import fs, {cpSync} from "node:fs";

const templatePath = join(dirname(fileURLToPath(import.meta.url)), 'template')


async function start() {
    const {projectName} = await prompts([
        {
            type: "text",
            name: "projectName",
            message: "Enter your project name",
            initial: "my-project",
            format: (val: string) => val.toLowerCase().split(" ").join("-"),
        },
    ])

    const isInCurrentDir = projectName.trim() === '.'
    const workDir = isInCurrentDir ? process.cwd(): join(process.cwd(), projectName)
    const resultProjectName = workDir.split('/').at(-1)

    const packageJsonText = fs.readFileSync(join(templatePath, 'package.json')).toString();
    const packageJson = JSON.parse(packageJsonText);

    cpSync(templatePath, workDir, {
        recursive: true,
    });

    fs.writeFileSync(join(workDir, 'package.json'), JSON.stringify({
        ...packageJson,
        name: resultProjectName
    }, null, 4));

    console.log('PROJECT SUCCESS CREATED');
}


start()