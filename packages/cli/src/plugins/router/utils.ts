import fs from "node:fs";
import {RouterElement} from "@/plugins/router/types.ts";

export const regex = /\[(.*?)]/g;

type RouterElementWithArray = RouterElement | RouterElementWithArray[]

export const resolveFiles = (paths: string, root: string): RouterElementWithArray[] => {
    const info: RouterElementWithArray[] = [];

    for (const path of fs.readdirSync(`${root}/${paths}`)) {
        const fileName = `${root}/${paths}/${path}`;

        const pagesPath = `${paths}/${path}`;

        if (fs.statSync(fileName).isDirectory()) {
            info.push(resolveFiles(pagesPath, root))
        } else {
            info.push({
                path: pagesPath,
                componentName: sanitizeName(pagesPath),
                countPath: (pagesPath.match(regex)?.map(e => e.slice(1, -1)) || []).length
            })
        }
    }

    return info.toSorted((path, path1) => {
        if (Array.isArray(path)) return -1;
        if (Array.isArray(path1)) return 1;

        if (path.path.endsWith('/index.kvr')) return 1;
        if (path1.path.endsWith('/index.kvr')) return -1;

        return path1.countPath - path.countPath;
    });
}

const flat = (elems: RouterElementWithArray[]): RouterElement[] => {
    const el: RouterElement[] = [];

    for (const item of elems) {
        if (Array.isArray(item)) {
            el.push(...flat(item))
        }
        else {
            el.push(item)
        }
    }

    return el;
}

export const resolveAllFiles = (root: string) => {
    // @ts-ignore
    return flat(resolveFiles('', root))
}

export const sanitizeName = (path: string) => {
    const replaced = path.replaceAll('/', '').replaceAll('[', '').replaceAll(']', '').replaceAll('-', '');

    return replaced.substring(0, replaced.length - 4);
}