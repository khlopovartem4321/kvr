import ts from "typescript";
import {resolveAllFiles} from "@/plugins/router/utils.ts";
import {
    callFunction,
    createArrow,
    createExport,
    createFileFromStatements,
    createImport,
    createObject
} from "@kvrjs/utils";
import {RouterElement} from "@/plugins/router/types.ts";

const createRouteFromFile = (file: RouterElement) => {
    return createObject({
        path: ts.factory.createStringLiteral(
            file.path.substring(0, file.path.length - 4)
        ),
        element: createArrow([], [
            ts.factory.createReturnStatement(
                ts.factory.createNewExpression(ts.factory.createIdentifier(file.componentName), undefined, [])
            )
        ])
    });
}

const getRootDocument = () => {
    return callFunction(ts.factory.createPropertyAccessExpression(
        ts.factory.createIdentifier("document"),
        ts.factory.createIdentifier("querySelector")
    ), [
        ts.factory.createStringLiteral("#root")
    ])
}

export const router = (pagesPath: string) => {
    const files = resolveAllFiles(pagesPath)

    return createFileFromStatements('', [
        createImport('@kvrjs/router', ['createRouter']),
        ...files.map((file) => createImport(`/src/pages/${file.path}`, file.componentName)),

        createExport(createArrow([], [
            ts.factory.createReturnStatement(
                callFunction('createRouter', [
                    ts.factory.createArrayLiteralExpression(
                        files.map(createRouteFromFile)
                    ),
                    getRootDocument()
                ]),
            )
        ])),
    ], ts.ScriptTarget.Latest)
}