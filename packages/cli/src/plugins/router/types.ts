


export interface RouterElement {
    path: string;
    countPath: number;
    componentName: string;
}