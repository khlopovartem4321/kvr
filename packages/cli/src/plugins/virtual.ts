import {Plugin, ViteDevServer} from "vite";


export const resolveModules = (server: ViteDevServer, ...element: string[]) => {
    const { moduleGraph, ws } = server

    for (const id of element) {
        const module = moduleGraph.getModuleById(id)
        if (module) {
            moduleGraph.invalidateModule(module)
        }
    }

    ws.send({
        type: 'full-reload',
        path: '*',
    })
}


export type VirtualMap = Record<string, string | (() => string)>


export const virtual = (element: VirtualMap): Plugin => {
    return {
        name: 'virtual',
        resolveId: (id) => {
            if (element[id] === undefined) return;

            return id;
        },
        load: (id) => {
            const candidate = element[id];

            if (candidate === undefined) return;

            return typeof candidate == 'function' ? candidate(): candidate;
        }
    }
}