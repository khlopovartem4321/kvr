import {Plugin} from "vite";
import ts, {CompilerOptions} from "typescript";
import {getComponentName, getPageComponentName, loadConfig} from "@kvrjs/utils";
import compile from '@kvrjs/compiler'
import {transpileFile} from "@kvrjs/utils";
import {VirtualMap} from "@/plugins/virtual.ts";
import {join} from "path";

export default ({pages, virtualModules}: {pages: string, virtualModules: VirtualMap}): Plugin => {
    let parsedConfig: CompilerOptions = null as unknown as CompilerOptions;
    let root = ''

    return {
        name: 'kvr-compiler',
        configResolved: (config) => {
            parsedConfig = loadConfig(config.root)
            root = config.root;
        },
        async transform(id: string, file: string) {
            if (!file.endsWith('.kvr')) {
                return
            }
            const target = parsedConfig.target ?? ts.ScriptTarget.Latest;

            const isPageDir = file.startsWith(pages);

            const componentName = isPageDir ? getPageComponentName(file, join(root, 'src') + '/') : getComponentName(file)
            const fileWithoutName = file.substring(0, file.lastIndexOf('/'));
            const {ts: code, css} = await compile(id,
                componentName,
                fileWithoutName,
                target);

            for (const [cssKey, cssValue] of Object.entries(css)) {
                virtualModules[join(fileWithoutName, `${componentName}.${cssKey}?inline`)] = cssValue.join('\n')
            }

            return {
                code: transpileFile(code, parsedConfig)
            };
        },
    }
}