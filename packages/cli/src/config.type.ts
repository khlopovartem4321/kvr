import {LoadConfigOptions} from "c12";

export interface Config {
    paths: {
        pages: string
    },
    server: {
        port: number
    },
    alias: Record<string, string>
}



export const defaultConfig: LoadConfigOptions<Config> = {
    name: 'kvr',
    defaults: {
        paths: {
            pages: 'pages'
        },
        server: {
            port: 5173
        },
        alias: {}
    }
}
