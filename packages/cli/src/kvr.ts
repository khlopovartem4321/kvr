#!/usr/bin/env node

import {cwd} from "process"
import {Config, defaultConfig} from "@/config.type.ts";
import {createServer} from "vite";
import {loadConfig} from "c12";
import {join} from "path";
import compiler from "@/plugins/compiler";
import {loadConfig as loadTsConfig, transpileFile} from '@kvrjs/utils'
import {resolveModules, virtual} from "@/plugins/virtual.ts";
import {fileURLToPath} from "node:url";
import path from "node:path";
import fs from "node:fs";
import {router} from "@/plugins/router";

export const VIRTUAL_ROUTER_MODULE = 'virtual-router'


async function start() {
    const {config} = await loadConfig<Config>(defaultConfig)

    const projectRoot = cwd();
    const executeFileRoot = path.dirname(fileURLToPath(import.meta.url));

    const pagesPath = join(projectRoot, 'src', config.paths.pages);
    const tsConfig = loadTsConfig(projectRoot);


    const virtualModules = {
        '/main.ts': fs.readFileSync(join(executeFileRoot, 'main.ts')).toString(),
        [VIRTUAL_ROUTER_MODULE]: () => transpileFile(router(pagesPath), tsConfig)
    };


    const server = await createServer({
        configFile: false,
        root: cwd(),
        server: {
            port: config.server.port,
        },
        plugins: [
            virtual(virtualModules),
            compiler({
                pages: pagesPath,
                virtualModules,
            }),
        ],
        resolve: {
            alias: config.alias,
        }
    });

    server.watcher.add(pagesPath)
    server.watcher.on('add', () => {
        resolveModules(server, VIRTUAL_ROUTER_MODULE);
    })

    await server.listen();
    server.printUrls()
    server.bindCLIShortcuts({print: true})
}


start()